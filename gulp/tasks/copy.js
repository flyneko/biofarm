var gulp   = require('gulp');
var config = require('../config.js');
var concat = require('gulp-concat');
var minify = require('gulp-minify');

gulp.task('copy:fonts', function() {
    return gulp
        .src(config.src.fonts + '/**/*.{ttf,eot,woff,woff2}')
        .pipe(gulp.dest(config.dest.fonts));
});

gulp.task('copy:js', function() {
    return gulp
        .src(config.src.js + '/**/*.*')
        .pipe(gulp.dest(config.dest.js));
});

gulp.task('copy:libs', function() {
    gulp.src([config.src.libs + '/_core/*.js', config.src.libs + '/other/**/*.js'])
        .pipe(concat('libs.js'))
        .pipe(minify({ ext: {min: '.js'}, noSource: true}))
        .pipe(gulp.dest(config.dest.js));
    gulp.src([config.src.libs + '/**/*.css'])
        .pipe(concat('libs.css'))
        .pipe(gulp.dest(config.dest.css));
});

gulp.task('copy:img', function() {
    return gulp
        .src([
            config.src.img + '/**/*.{jpg,png,jpeg,svg,gif}',
            '!' + config.src.img + '/svgo/**/*.*'
        ])
        .pipe(gulp.dest(config.dest.img));
});

gulp.task('copy', [
    'copy:img',
    'copy:fonts',
    'copy:js',
    'copy:libs'
]);

gulp.task('copy:watch', function() {
    gulp.watch(config.src.img + '/**/*.*', ['copy:img']);
    gulp.watch(config.src.js + '/**/*.*', ['copy:js']);
    gulp.watch(config.src.libs + '/**/*.*', ['copy:libs']);
});
