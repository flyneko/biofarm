var gulp        = require('gulp');
var runSequence = require('run-sequence');
var config      = require('../config');

gulp.task('default', function(cb) {
    runSequence(
        'nunjucks',
        'iconfont',
        'sass',
        'sprite:svg',
        'sprite:png',
        'watch',
        'copy',
        cb
    );
});
