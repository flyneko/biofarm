var gulp         = require('gulp');
var sass         = require('gulp-sass');
var config         = require('../config');
var postcss      = require('gulp-postcss');
var autoprefixer = require('autoprefixer');
var mqpacker     = require('css-mqpacker');
var inline_base64 = require('gulp-inline-base64');

var processors = [
    autoprefixer({
        browsers: ['last 4 versions'],
        cascade: false
    })
];
gulp.task('sass', function() {
    return gulp
        .src(config.src.sass + '/*.sass')
        .pipe(sass({
            outputStyle: config.production ? 'compact' : 'expanded', // nested, expanded, compact, compressed
            precision: 5
        }))
        .pipe(inline_base64({
            baseDir: config.src.sass + '/',
            maxSize: 1
        }))
        .pipe(postcss(processors))
        .pipe(gulp.dest(config.dest.css));
});

gulp.task('sass:watch', function() {
    gulp.watch(config.src.sass + '/**/*.sass', ['sass']);
});

function isMax(mq) {
    return /max-width/.test(mq);
}

function isMin(mq) {
    return /min-width/.test(mq);
}

function sortMediaQueries(a, b) {
    A = a.replace(/\D/g, '');
    B = b.replace(/\D/g, '');

    if (isMax(a) && isMax(b)) {
        return B - A;
    } else if (isMin(a) && isMin(b)) {
        return A - B;
    } else if (isMax(a) && isMin(b)) {
        return 1;
    } else if (isMin(a) && isMax(b)) {
        return -1;
    }

    return 1;
}
