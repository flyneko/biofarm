var util = require('gulp-util');

var production = util.env.production || util.env.prod || false;
var destPath = 'build';

var config = {
    env       : 'development',
    production: production,

    src: {
        root         : 'src',
        templates    : 'src/nunj',
        templatesData: 'src/nunj/data',
        sass         : 'src/sass',
        sassGen      : 'src/sass/generated',
        libs         : 'src/libs',
        fonts        : 'src/fonts',
        js           : 'src/js',
        img          : 'src/img',
        svgIcons     : 'src/svg-icons',
        svgFonts     : 'src/svg-fonts',
        pngIcons     : 'src/png-icons'

    },
    dest: {
        root : destPath,
        html : destPath,
        css  : destPath + '/assets/css',
        js   : destPath + '/assets/js',
        img  : destPath + '/assets/img',
        fonts: destPath + '/assets/fonts'
    },

    setEnv: function(env) {
        if (typeof env !== 'string') return;
        this.env = env;
        this.production = env === 'production';
        process.env.NODE_ENV = env;
    },

    logEnv: function() {
        util.log(
            'Environment:',
            util.colors.white.bgRed(' ' + process.env.NODE_ENV + ' ')
        );
    },

    errorHandler: require('./util/handle-errors')
};

config.setEnv(production ? 'production' : 'development');

module.exports = config;
