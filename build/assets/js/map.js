var Map = {
    init: function () {
        this.create(document.getElementById('map'), [55.725648, 37.626248]);
    },

    create: function (elem, coords) {
        var uluru = {lat: coords[0], lng: coords[1]};
        var map = new google.maps.Map(elem, {
            zoom: 17,
            center: uluru,
            disableDefaultUI: true,
            gestureHandling: 'cooperative',
            styles: [{ "featureType": "poi", "stylers": [{ "visibility": "off"}]}, { "featureType": "transit", "stylers": [{ "visibility": "off"}]}]
        });
        var marker = new google.maps.Marker({
            position: uluru,
            map: map,
            icon: 'assets/img/map-marker.svg',
        });
    }
}