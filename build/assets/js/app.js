var isMobile = function () { return $('.js-nav-toggle').is(':visible'); }

$(function () {
    (function () {
        SwiperProxy($('.js-main-carousel'), {
            dots: false,
            nav: true,
            slidesPerView: 1,
            paginationClickable: true,
            speed: 600,
            loop: true,
            spaceBetween: 0,
            breakpoints: {
                480: { spaceBetween: 8 }
            }
        });
    })();

    (function () {
        var cartImage = function () {
            var img = [];
            $('.certs').each(function () {
                var $certs = $(this);
                if ($certs.data('gallery-init'))
                    return;

                $certs.data('gallery-init', true);
                $certs.find('.js-cert-link').each(function () {
                    img.push({
                        src: $(this).attr('href'),
                        thumb: $(this).attr('href')
                    })
                }).click(function () {
                    $certs.lightGallery({
                        dynamic: true,
                        dynamicEl: img,
                        thumbnail:true,
                        thumbMargin: 10,
                        hideBarsDelay: 999999999,
                        download: false,
                        toogleThumb: false,
                        counter: false,
                        index: $(this).parents('.swiper-slide:first').length ?$(this).parents('.swiper-slide:first').index() : $(this).parent().index()
                    }).on('onCloseAfter.lg', function () {
                        if ($certs.data('lightGallery'))
                            setTimeout(function() { $certs.data('lightGallery').destroy(true); $certs.off(); }, 100);
                    });;
                    return false;
                });


            })
        }
        SwiperProxy($('.js-certs-carousel'), {
            dots: false,
            nav: true,
            slidesPerView: 5,
            paginationClickable: true,
            speed: 600,
            loop: false,
            spaceBetween: 30,
            breakpoints: {
                940: { slidesPerView: 4 },
                760: { slidesPerView: 3, spaceBetween: 15 },
                640: { slidesPerView: 2 },
                480: { slidesPerView: 1 }
            },
        });

        cartImage();
    })();

    (function () {
        SwiperProxy($('.js-items-carousel'), {
            dots: false,
            nav: true,
            slidesPerView: 1,
            paginationClickable: true,
            speed: 600,
            loop: true,
            spaceBetween: 30,
            breakpoints: {
                480: { spaceBetween: 8 }
            }
        });
    })();

    (function () {
        var showMenu = function ($li) {
            var $sub = $li.find('> ul');
            var $items = $sub.find('.sidebar__sub-item');

            if ($sub.length) {
                if (!$sub.is(':visible')) {
                    $sub.slideDown(null, function () {
                        $('.js-aside').trigger("sticky_kit:recalc");
                        $items.addClass('is-showing').css('animation-duration', '0.3s');
                        setTimeout(function () {
                            $items.css('opacity', 1).removeClass('is-showing');
                            $li.addClass('is-active');
                        }, 300 + ($items.length * 100));
                    });
                } else {
                    $items.addClass('is-hiding').css('animation-duration', '0.3s');
                    setTimeout(function () {
                        $li.removeClass('is-active');
                        $sub.slideUp(null, function () {
                            $('.js-aside').trigger("sticky_kit:recalc");
                            $items.removeClass('is-hiding').attr('style', '').prop('stlye', '');
                        });
                    }, 300 + ($items.length * 100));

                }

                return true;
            }
            return false;
        };
        $('.js-sidebar-item').click(function (e) {
            var $target = $(e.target);
            var $li = $(this);
            if (!$target.parent().hasClass('sidebar'))
                return true;

            if (showMenu($li)) {
                return false;
            }
        });
        $('.js-sidebar-item > a').click(function () {
            var $li = $(this).parents('li:first');
            if (showMenu($li)) {
                return false;
            }
        });
    })();

    (function () {
        if ($('.js-aside').is(':visible'))
            $('.js-aside').stick_in_parent({
                parent: '[data-sticky_parent]',
                offset_top: 80
            });
    })();

    (function () {
        $(".js-range").ionRangeSlider({
            type: "double",
            force_edges: true,
            hide_from_to: true,
            step: 100,
            onChange : function (data) {
                data.input.nearest('[data-range=from]').val(data.from);
                data.input.nearest('[data-range=to]').val(data.to);
            },
        });
    })();

    (function () {
        // Select2
        $('.js-select2').each(function () {
            var state = function (state) {
                var icon = $(state.element).data('icon');
                return icon ? $('<span>' + '<svg class="icon icon-' + icon + '"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="assets/img/sprite.svg#icon-' + icon + '"></use></svg>' + state.text + '</span>') : state.text;
            };
            $(this).select2({
                minimumResultsForSearch: Infinity,
                width: '100%',
                templateResult: state,
                templateSelection: state
            });
        });

        $('.js-product-select').each(function () {
            var state = function (state) {
                var id = $(state.element).data('id');
                return $('' +
                    '<div clas="product-select__option">' +
                    '<div class="product-select__option-inn">' +
                    '<div class="product-select__option-value">' + state.text + '</div>' +
                    (id ? '<div class="product-select__option-id">' + id + '</div>' : '') +
                    '</div>' +
                    '</div>');
            };
            $(this).select2({
                minimumResultsForSearch: Infinity,
                width: '100%',
                containerCssClass: 'product-select',
                dropdownCssClass: 'product-select__dropdown',
                templateResult: state,
                templateSelection: state
            });
        });
    })();

    // Header
    (function () {
        var $header = $(".header");
        var pos = $header.offset().top + $header.outerHeight();
        var $replace = $('<div>').insertBefore($header);
        var $headerSearch = $('.js-header-search');
        function headerFixed() {
            var top = $(document).scrollTop();
            if (isMobile())
                return;
            if (top > 0 && top > pos && !$header.hasClass('is-fixed')) {
                pos = $header.offset().top + $header.outerHeight();
                $replace.css('height', $header.outerHeight())
                $header.addClass('is-fixed');
            } else if (top <= pos - 100 && $header.hasClass('is-fixed')) {
                $replace.css('height', 0);
                $header.removeClass('is-fixed');
                $headerSearch.hide();
            }
        }


        $(window).scroll(headerFixed);
        headerFixed();

        $('.js-mnav-link').click(function () {
            var $this = $(this);
            var $li = $this.parent();
            var $sub = $li.find('> ul');

            if (!$sub.length)
                return true;

            $sub.slideToggle();
            return false;
        })

        $('.js-header-search-toggle').click(function () {
            $headerSearch.fadeIn();
            $headerSearch.find('input').focus();
            return false;
        });

        $headerSearch.find('input').focusout(function () {
            $headerSearch.fadeOut();
        })
    })();

    (function () {
        var $main = $('.js-main');
        var $header = $(".header");
        var menus = [
            {obj: $('.js-m-aside'), side: 'left', toggle: $('.js-m-aside-toggle')},
            {obj: $('.js-m-nav'), side: 'right', toggle: $('.js-nav-toggle')}
        ];

        for (var i in menus) {
            var el = menus[i];
            (function (el) {
                var slideout = new Slideout({
                    panel: $main[0],
                    menu: el.obj[0],
                    padding: el.obj.width(),
                    tolerance: 70,
                    duration: 600,
                    easing: 'cubic-bezier(0.175, 0.885, 0.32, 1.275)',
                    touch: false,
                    side: el.side
                });
                var open = function() {
                    el.obj.show();
                    el.toggle.addClass('is-active');
                    $header.css('position','absolute').css('top', ($(window).scrollTop()) + 'px');
                }
                slideout.on('translatestart', open);
                slideout.on('beforeopen', open);
                slideout.on('close', function() { $('html').removeClass('slideout-closing'); $header.attr('style', '').prop('style', ''); el.obj.hide(); });
                slideout.on('beforeclose', function () { $('html').addClass('slideout-closing'); el.toggle.removeClass('is-active'); });

                el.toggle.click(function () {
                    if (isMobile())
                        slideout.open();
                    else
                        el.toggle.toggleClass('is-active');
                    return false;
                });

                $main.click(function () {
                    if (slideout.isOpen()) {
                        slideout.close();
                        return false;
                    }
                });
            })(el);
        }
    })();

    (function () {
        $('.js-item-count').itemCount();
    })();

    (function () {
        var $cart = $('.js-header-cart')
        var $toggle = $('.header .js-cart-toggle');
        $toggle.click(function () {
            if (isMobile())
                return true;
            $toggle.toggleClass('is-active');
            $cart.toggleClass('is-active').css('top', $toggle.position().top + $toggle.height());
            return false;
        });

        $('html').click(function (e) {
            var $t = $(e.target);
            if (!$t.parents('.js-header-cart').length) {
                $toggle.removeClass('is-active');
                $cart.removeClass('is-active');
            }
        })
    })();

    (function () {
        $('.js-tabs').tabs();
    })();

    (function () {
        var $parent = $('.item');
        var $gallery = $parent.find('.js-item-gallery');
        if (!$parent.length)
            return;
        var galleryTop = SwiperProxy($gallery, {
            dots: false,
            nav: true,
            direction: 'vertical',
            slidesPerView: 1,
            paginationClickable: true,
            spaceBetween: 30,
            mousewheelControl: true,
            speed: 600,
            onClick: function () {
                var img = [];
                $gallery.find('img').each(function () {
                    img.push({
                        src: $(this).attr('src'),
                        thumb: $(this).attr('src')
                    })
                });

                $gallery.lightGallery({
                    dynamic: true,
                    dynamicEl: img,
                    thumbnail:true,
                    thumbMargin: 10,
                    hideBarsDelay: 999999999,
                    download: false,
                    toogleThumb: false,
                    counter: false,
                    index: $gallery.find('.swiper-slide-active').index()
                }).on('onCloseAfter.lg', function () {
                    if ($gallery.data('lightGallery'))
                        setTimeout(function() { $gallery.data('lightGallery').destroy(true); $gallery.off(); }, 100);
                });
            },
            breakpoints: {
                760: { direction: 'horizontal' },
            },
        });
        var galleryThumbs = new Swiper($parent.find('.js-item-gallery-thumbs'), {
            spaceBetween: 10,
            centeredSlides: true,
            slidesPerView: 'auto',
            speed: 600,
            touchRatio: 0.2,
            slideToClickedSlide: true,
        });
        galleryTop.params.control = galleryThumbs;
        galleryThumbs.params.control = galleryTop;
    })();

    (function () {
        $('.js-product-add-cart').click(function () {
            var $this = $(this);
            var $product = $(this).parents('.product:first');
            $this.addClass('is-active').attr('disabled', true).prop('disabled', true);
            $product.addClass('is-added');
            if ($this.text().trim() != '')
                $this.find('span').text('Товар добавлен к корзину');
            return false;
        });
    })();

    (function () {
        var $kit = $('.js-product-kit');
        if ($kit.length) {
            var totalWidth = 0;
            $kit.find('> *').each(function(index) {
                totalWidth += parseInt($(this).outerWidth());
            });
            new IScroll($kit.css('width', totalWidth).wrap('<div>').parent()[0], { scrollX: true, scrollY: false, mouseWheel: true });
        }
    })();

    // Complex input
    (function () {
        $('.complex-input').each(function () {
            var $parent = $(this);
            $parent.find('input, textarea').focus(function () {
                $parent.addClass('is-focus');
            }).focusout(function () {
                $parent.removeClass('is-focus');
            });
        });
    })();

    (function () {
        $('.js-search-toggle').focus(function () {
            var $search = $(this).parent();
            if (!$search.hasClass('is-init'))
                $search.addClass('is-active');
            return false;
        }).focusout(function () {
            var $search = $(this).parent();
                $search.removeClass('is-active');
        })
    })();

    (function () {
        var init = function () {
            $('.js-replace').each(function () {
                var $place = $(this);
                var targetClass = $place.data('target');
                var parent = $place.data('parent');
                var placeWidth = parseInt($place.data('width'));
                var $target = parent ? $place.parents(parent).find(targetClass) : $(targetClass);

                if (window.innerWidth <= placeWidth) {
                    $place.show()
                    if (!$place.data('old-place'))
                        $place.data('old-place', $target.wrap('<div>').parent());
                    $target.appendTo($place);
                } else {
                    $place.hide();
                    if ($place.data('old-place'))
                        $target.appendTo($place.data('old-place'));

                }
            });
        }

        init();
        $(window).resize(init);
    })();

    (function () {
        $('.js-animate-number').each(function () {
            var $this = $(this);

            var easingFn = function(t, b, c, d) {
                var ts = (t /= d) * t;
                var tc = ts * t;
                return b + c * (tc + -3 * ts + 3 * t);
            }
            var options = {
                useEasing: true,
                easingFn: easingFn,
            };
            var demo = new CountUp(this, 0, $this.data('num'), 0, 3, options);
            if (!demo.error)
                demo.start();
        });
    })();

    (function () {
        var init = function () {
            $('.js-dotdotdot').each(function () {
                var $this = $(this);
                var opt = {
                    ellipsis	: '... '
                }
                if ($this.data('after'))
                    opt.after = $this.data('after');
                $this.dotdotdot(opt);
            })
        }
        init();
        $(window).resize(function () {
            $('.js-dotdotdot').trigger("update.dot");
        });
    })();
});
